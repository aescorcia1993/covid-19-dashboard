# testservi

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

El proyecto esta elaborado en VUE 2, con una version de NODE V10.19.0
Se utilizaron los siguientes plugin que se pueden observar en el package Json

"dependencies"=
"@googlemaps/js-api-loader": "^1.12.1",
"bootstrap-vue": "^2.16.0",
"core-js": "^3.6.5",
"vue": "^2.6.11",
"vue-multiselect": "^2.1.6",
"vue2-google-maps": "^0.10.7"

El servicio se hizo con Fetch Nativo de Javascript
Se utilizo Boostrap Vue para manejo de la UI.
No se observaran pacientes Activos porque se utilizo el campo "estado" y solo entrega:
- Fallecido
- Recuperado
Debido a que la consulta esta hasta abril del año 2020.