import Vue from 'vue'
import App from './App.vue'

//bootstrap vue
import { BootstrapVue, BootstrapVueIcons  } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)

//Google Maps
import * as VueGoogleMaps from 'vue2-google-maps'
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyCpDPcQvZMgKTnrWfAumrW4yyariu45Mr0'
  }
})

Vue.config.productionTip = false


new Vue({
  render: h => h(App),
}).$mount('#app')
